# Summary

* [Introduction](README.md)
* [第1章 环境搭建](chapter01/README.md)

  * [docker搭建LNMP环境](chapter01/dlnmp.md)
  * [phpStudy开发环境](chapter01/phpstudy.md)
  * [Linux系统搭建LNMP环境](chapter01/linux.md)

* [第2章 开发工具](chapter02/README.md)

  * [PhpStorm IDE开发环境](chapter02/phpstorm.md)
  * [Navcat MySQL可视化管理](chapter02/navcat.md)
  * [Git版本管理](chapter02/git.md)

* [第3章 代码规范](chapter03/README.md)
  * [PHP-FIG介绍](chapter03/profile.md)
  * [PSR-1规范](chapter03/psr-1.md)
  * [PSR-2规范](chapter03/psr-2.md)
  * [PSR-3规范](chpater03/psr-3.md)
  * [PSR-4规范](chapter03/psr-4.md)
  * [整洁代码之道](chapter03/clear-code.md)
    * 命名
    * 函数
    * 注释
    * 格式
    * 对象和数据结构
    * 错误处理
    * 单元测试
    * 类
  * [规范代码示例](chapter03/full.md)

* [第4章 ThinkPHP](chapter04/README.md)

  * ThinkPHP介绍
  * MVC简介
  * [安装](chapter04/install.md)
  * [简单使用](chapter04/simple-usage.md)

* [第5章 网站前台](chapter05/README.md)

  * 集成Bootstrap
  * 网站菜单
  * 滑动幻灯片
  * 网格使用

* [第6章 网站后台](chapter06/README.md)

  * 集成Metis后台模板
  * 站点管理
  * 站点信息
  * 管理员管理
  * 管理员登录

* [第7章 文章管理](chapter07/README.md)

  * 文章管理
  * 文章分类
  * 标签管理

* [第8章 文件管理](chapter08/README.md)

  * 集成elfinder文件管理系统

* [第9章 产品管理](chapter09/README.md)

  * 产品分类
  * 产品属性管理
  * 产品管理

* [第10章 购物车与结算](chapter10/README.md)

  * Session购物车
  * 购物车页面
  * 结算

* 第11章 支付系统\(chapter11\/README.md\)

  * 集成支付宝
  * 集成微信支付
  * 集成Paypal支付

* [第12章 订单处理](chapter12/README.md)

  * 订单状态
  * SMTP邮件发送配置
  * 邮件模板

* [第13章 用户管理](chapter13/README.md)

  * [MySQL数据库设计](chapter6/db.md)
  * [用户注册](chapter6/register.md)
  * [用户登陆](chapter6/login.md)
  * [用户登出](chapter6/logout.md)
  * [个人中心](chapter6/ucenter.md)
  * 用户管理

* [第14章 RABC后台权限](chapter14/README.md)

  * 管理员管理
  * 角色管理
  * 权限分配

* [第15章 调试PHP](chapter15/README.md)

  * 安装Xdebug
  * 配置phpStorm
  * 设置断点及调试

* [第16章 安全](chapter16/README.md)

  * https
  * MYSQL安全
  * 跨站脚本攻击

* [第17章 高性能优化](chapter17/README.md)

  * ab测试
  * 使用MySQL索引
  * php.ini优化
  * my.cnf优化
  * nginx配置优化
  * 使用OPCache
  * 页面静态化
  * Redis缓存
  * CDN使用


